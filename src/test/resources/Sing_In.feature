Feature: Sing In
    "**sign in to web after sign up**"

  Scenario Outline: Successful login to the website
    "**success login with valid credentials**"
    Given I am on jpet web login page
    When I navigate to sign in
    And I enter username "<usr_name>"
    And I enter password "<passwd>"
    And I click login button
    Then I can see the web page home screen

    Examples:
      | usr_name | passwd |
      | mtk | mtk123 |

  Scenario Outline: Unsuccessful login to the website using invalid credentials
    "**unsuccess login using invalid credentials**"
    Given I am on jpet web login page
    When I navigate to sign in
    And I enter username "<inv_usr_name>"
    And I enter password "<inv_passwd>"
    And I click login button
    Then I can not login to the web page

    Examples:
    | inv_usr_name | inv_passwd |
    | kkt          | kkt123     |



